package code;
import javafx.*;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class TicTacToeButton extends Button{
    
	private static int numberTaken;
	private int id;
	
    public TicTacToeButton(String title, int id){
        super(title);
        this.id = id;
        numberTaken = 0;
    }
    
    public void change(){
        if (this.getText().equals("-")){
            this.setText("X");
            this.setTextFill(Color.BLUE);
            ++numberTaken;
            id = 1;
            return;
        }
        if (this.getText().equals("X")){
            this.setText("O");
            this.setTextFill(Color.GREEN);
            id = -1;
            return;
        }
        if (this.getText().equals("O")){
            this.setText("-");
            return;
        }
    }
    
    public static int getNumberTaken(){
    	return numberTaken;
    }

    public int getID(){
    	return id;
    }
}