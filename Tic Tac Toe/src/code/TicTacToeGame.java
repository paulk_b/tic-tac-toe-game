package code;
import java.util.Optional;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class TicTacToeGame extends Application implements EventHandler<ActionEvent>{
	private TicTacToeButton[] buttons;
	private TicTacToeBoard board;
	private Scene scene1; //start screen
	private Scene scene2; //game screen
	private Scene question; //first or second?
	private static Boolean done;
	
	public static void main(String [] args){
		launch(args);
	}
	
	@Override
	public void start(Stage primary) throws Exception{	
		done = false;
		//START SCREEN
		Label top = new Label("Welcome to Tic-Tac-Toe!\n Click OK to start the game or cancel to exit!");
		top.setTextFill(Color.BLUEVIOLET);
		top.setPrefSize(250, 250);
		Button ok = new Button("OK");
		Button cancel = new Button("Cancel");
		ok.setOnAction(e -> primary.setScene(question));
		cancel.setOnAction(e -> System.exit(1));
		//creating layout and adding buttons
		BorderPane layout = new BorderPane();
		HBox hbox = new HBox(25);
		hbox.getChildren().addAll(ok, cancel);
		VBox vbox = new VBox(25);
		vbox.getChildren().addAll(top);
		layout.setBottom(hbox);
		layout.setCenter(vbox);
		layout.setPadding(new Insets(20, 10, 10, 50));
		scene1 = new Scene(layout, 400, 200);
		
		//QUESTION
		Label top2 = new Label("Would you like to go first? Or second?");
		top2.setTextFill(Color.BLUEVIOLET);
		top2.setPrefSize(250, 250);
		Button first = new Button("First!");
		Button second = new Button("Second!");
		Button cancel2 = new Button("Cancel");
		first.setOnAction(e -> {
			primary.setScene(scene2);
		});
		second.setOnAction(e -> {
			primary.setScene(scene2);
			computerGo();
		});
		cancel2.setOnAction(e -> System.exit(1));
		//creating layout and adding buttons
		BorderPane layout2 = new BorderPane();
		HBox hbox2 = new HBox(25);
		hbox2.getChildren().addAll(first, second, cancel2);
		VBox vbox2 = new VBox(25);
		vbox2.getChildren().addAll(top2);
		layout2.setBottom(hbox2);
		layout2.setCenter(vbox2);
		layout2.setPadding(new Insets(20, 10, 10, 50));
		question = new Scene(layout2, 400, 200);
		
		//ACTUAL GAME 
		//initializes variables
		buttons = new TicTacToeButton[9];
		board = new TicTacToeBoard();
		//creates buttons
		for(int i = 0; i < buttons.length; i++){
			buttons[i] = new TicTacToeButton("-", 0);
			buttons[i].setPrefSize(100, 100);
			buttons[i].setOnAction(this);
		}
		//adds buttons to board
		int index = 0;
		for(int row = 0; row < 3; row++){
			for(int col = 0; col < 3; col++){
				board.add(buttons[index], col, row);
				++index;
			}
		}
		scene2 = new Scene(board, 500, 500);
		
		//puts together scene 2 and scene 1
		primary.setTitle("Tic-Tac-Toe Game!"); 
		primary.setScene(scene1);
		primary.show();
	}
	
	@Override
	public void handle(ActionEvent event) {
		if(TicTacToeButton.getNumberTaken() != 9){
			for(int i = 0; i < buttons.length; i++){
				if(event.getSource() == buttons[i]){
						haveWinner();
						buttons[i].change();
						haveWinner();
						computerGo();
						haveWinner();;
					}		
				}
			}
		else{
			haveWinner();
		}
	}
	
	
	public void computerGo(){
    	int pick = (int)(Math.random()*8);
    	if(buttons[pick].getText().equals("-")){
    		buttons[pick].change();
    		buttons[pick].change();
    	}
    	else{
    		computerGo();
    	}
    }
	
	public void end(int winner){
		Alert alert = new Alert(AlertType.CONFIRMATION); 
		if(winner == 1){
			alert.setTitle("You have won!");
			alert.setHeaderText("Congratulations on winning!"); 
		}
		else if(winner == 2){
			alert.setTitle("You have lost!");
			alert.setHeaderText("Maybe next time..."); 
		}
		else{
			alert.setTitle("You have tied!");
			alert.setHeaderText("Nobody won this time!");
		}		
		alert.showAndWait();
		System.exit(1);
	}
	
	public void haveWinner(){ //checks to see if there's a winner
		//check: (0, 1, 2) (3, 4, 5) (6, 7, 8) (0 3 6) (1 4 7) (2 5 8) (0 4 8) (6 4 2)
		//1 is person won, 2 is computer one, -1 is tie
		if((buttons[0].getID() + buttons[1].getID() + buttons[2].getID()) == 3){
				end(1);
		}
		if((buttons[0].getID() + buttons[1].getID() + buttons[2].getID()) == -3){
				end(2);
		}
		if((buttons[3].getID() + buttons[4].getID() + buttons[5].getID()) == 3){
				end(1);
		}
		if((buttons[3].getID() + buttons[4].getID() + buttons[5].getID()) == -3){
				end(2);
		}	
		if((buttons[6].getID() + buttons[7].getID() + buttons[8].getID()) == 3){
				end(1);
		}
		if((buttons[6].getID() + buttons[7].getID() + buttons[8].getID()) == -3){
				end(2);
		}
		if((buttons[0].getID() + buttons[3].getID() + buttons[6].getID()) == 3){
				end(1);
		}
		if((buttons[0].getID() + buttons[3].getID() + buttons[6].getID()) == -3){
				end(2);
		}
		if((buttons[1].getID() + buttons[4].getID() + buttons[7].getID()) == 3){
				end(1);
		}
		if((buttons[1].getID() + buttons[4].getID() + buttons[7].getID()) == -3){
				end(2);
		}
		if((buttons[2].getID() + buttons[5].getID() + buttons[8].getID()) == 3){
				end(1);
		}
		if((buttons[2].getID() + buttons[5].getID() + buttons[8].getID()) == -3){
				end(2);
		}
		if((buttons[0].getID() + buttons[4].getID() + buttons[8].getID()) == 3){
				end(1);
		}
		if((buttons[0].getID() + buttons[4].getID() + buttons[8].getID()) == -3){
				end(2);
		}
		if((buttons[6].getID() + buttons[4].getID() + buttons[2].getID()) == 3){
				end(1);
		}
		if((buttons[6].getID() + buttons[4].getID() + buttons[2].getID()) == -3){
				end(2);
		}
		if(TicTacToeButton.getNumberTaken() == 9){
			end(-1);
		}
		else{
			return;
		}

	}

	
}
